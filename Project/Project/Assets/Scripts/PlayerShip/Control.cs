﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;

public class Control : MonoBehaviour
{
    [SerializeField] private float speedShip = 10;

    private Vector2 moveship = Vector2.zero;
    private Vector2 infoship = Vector2.zero;


    void Update()
    {
        movekeys();
    }

    private void movekeys()
    {
        infoship = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        infoship = infoship.normalized;
        var newXAxis = transform.position.x + infoship.x * Time.deltaTime * speedShip;
        var newYAxis = transform.position.y + infoship.y * Time.deltaTime * speedShip;
        transform.position = new Vector2(newXAxis, newYAxis);
    }
}
