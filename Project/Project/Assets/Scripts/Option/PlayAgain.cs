﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayAgain : MonoBehaviour
{
    [SerializeField] private AudioSource sound;
    [SerializeField] private AudioClip soundShoot;
    [SerializeField] private float volume;
    
    void Start()
    {
        Button button = gameObject.GetComponent<Button>();
        sound.PlayOneShot(soundShoot);
        button.onClick.AddListener(RestartGame);
    }
    public void RestartGame()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
}
