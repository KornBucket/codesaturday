﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGun : MonoBehaviour
{
    [SerializeField] private float bulletSpeed;
    Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = -transform.up * bulletSpeed;
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTrigger(Collider other)
    {
        Destroy(gameObject);
    }
    private void OnInvisible()
    {
        Destroy(gameObject);
    }
}
