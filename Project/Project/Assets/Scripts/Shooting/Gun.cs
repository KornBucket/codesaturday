﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField] private AudioSource sound;
    [SerializeField] private AudioClip soundShoot;
    [SerializeField] private AudioClip soundUlt;
    [SerializeField] private GameObject singleBullet;
    [SerializeField] private GameObject final;
    [SerializeField] private Transform bullet;
    
    [SerializeField] private float volume;
    [SerializeField] public float rateOfFire;

    private float nextFireRate;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time > nextFireRate)
        {
            nextFireRate = Time.time + rateOfFire;
            Instantiate(singleBullet, bullet.position, bullet.rotation);
            sound.PlayOneShot(soundShoot,volume);
        }
        if (Input.GetButton("Fire2") && Time.time > nextFireRate)
        {
            nextFireRate = Time.time + rateOfFire;
            Instantiate(final, bullet.position, bullet.rotation);
            sound.PlayOneShot(soundUlt, volume);
        }
    }
}
