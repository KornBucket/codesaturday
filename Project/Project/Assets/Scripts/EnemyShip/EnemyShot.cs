﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShot : MonoBehaviour
{
    [SerializeField] private AudioSource sound;
    [SerializeField] private AudioClip soundShoot;
    [SerializeField] private GameObject enemyBullet;
    [SerializeField] private Transform spawnOfBullet;
    [SerializeField] private float volume;
    private float time0 = 0;
    private float time1 = 1;

    private void shootfromenemy()
    {
        time0 -= Time.deltaTime;
        if (time0 <= 0)
        {
            Instantiate(enemyBullet, spawnOfBullet.position, spawnOfBullet.rotation);
            time0 = time1;
        }
    }
    // Update is called once per frame
    void Update()
    {
        shootfromenemy();
    }
}
