﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy01 : MonoBehaviour
{
    [SerializeField] private Transform enemyShip;
    [SerializeField] private float speedEnemyShip;
    private float distancemove = 1.5f;


    // Update is called once per frame
    void Update()
    {
        enemyshipmove();
    }
    private void enemyshipmove()
    {
        Vector2 directenemytoplayer = enemyShip.position - transform.position;
        Vector2 directmove = directenemytoplayer.normalized;
        Vector2 directfollowplayer = directmove * speedEnemyShip;

        if(directenemytoplayer.magnitude > distancemove)
        {
            transform.Translate(directfollowplayer * Time.deltaTime);
        }
    }
}
